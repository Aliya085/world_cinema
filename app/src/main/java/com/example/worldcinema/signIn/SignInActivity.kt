package com.example.worldcinema.signIn

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.worldcinema.R
import com.example.worldcinema.databinding.ActivitySignInBinding
import com.example.worldcinema.main.MainActivity
import com.example.worldcinema.signup.SignUpActivity

class SignInActivity : AppCompatActivity() {
    private lateinit var binding : ActivitySignInBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivitySignInBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        binding.registrationButton.setOnClickListener{
            val intent = Intent(this, SignUpActivity :: class.java)
            startActivity(intent)
            finish()
        }

        binding.enterButton.setOnClickListener {
            val intent = Intent(this, MainActivity:: class.java)
            startActivity(intent)
            finish()

        }
    }
}